#!/usr/bin/env bash
echo -e "\n\n\n ------- Creating inital users and roles ----------\n\n\n"
ansible-playbook ./cicd_create.yml  -vvv 
echo "\n\n\n ------- Creating staging environment ----------\n\n\n"
ansible-playbook ./create_env.yml --extra-vars @env_config/staging.yml -vvv
echo $?
echo -e "\n\n\n ------- Creating staging dashboard ----------\n\n\n"
sh generate_dashboard.sh staging > dashboards/dashboard-staging.json
echo $?
echo -e "\n\n\n ------- putting dashboard on cloudwatch staging ----------\n\n\n"
aws cloudwatch put-dashboard --dashboard-name "node3tierapp-staging" --dashboard-body file://dashboards/dashboard-staging.json
echo $?
echo -e "\n\n\n ------- Creating production environment ----------\n\n\n"
ansible-playbook ./create_env.yml --extra-vars @env_config/prod.yml
echo $? 
echo -e "\n\n\n ------- Creating production dashboard ----------\n\n\n"
sh generate_dashboard.sh prod > dashboards/dashboard-prod.json
echo $? 
echo -e "\n\n\n ------- putting dashboard on cloudwatch prod ----------\n\n\n"
aws cloudwatch put-dashboard --dashboard-name "node3tierapp-prod" --dashboard-body file://dashboards/dashboard-prod.json
 