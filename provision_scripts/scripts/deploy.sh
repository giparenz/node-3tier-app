#!/usr/bin/env bash
NAME="deploy.sh"
env=$1
usage() {
    echo "./deploy.sh <ENV> "
    echo "ENV - staging or prod"
    exit
}


echo " pipeline "
echo "-- TEST --"
echo " build  "
docker-compose build 
docker-compose up -d 
echo " test TEST "

if [ $( ss -tnl | grep -c 5432 ) -lt 2 ] ; then 
   echo "please start postgres"
   exit 1 
fi 

curl http://localhost/ > /dev/null 2>&1
if [ $? -ne 0 ]; then 
   echo "TEST in TEST has failed"
   exit 1
fi 

curl http://localhost:3000/api/status > /dev/null 2>&1
if [ $? -ne 0 ]; then 
   echo "TEST in TEST has failed"
   exit 1
fi 

docker-compose down 

echo " pushing images to AWS "

login=$(aws ecr get-login --no-include-email)
eval $login

docker tag node3tierapp_api:latest ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/node3tierapp_api:latest
if [ $? -ne 0 ]; then 
   echo " ERROR taging image api "
   exit 1
fi 

docker tag node3tierapp_web:latest ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/node3tierapp_web:latest
if [ $? -ne 0 ]; then 
   echo " ERROR taging image web "
   exit 1
fi 

docker push ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/node3tierapp_api:latest
if [ $? -ne 0 ]; then 
   echo " ERROR pushing api image to aws "
   exit 1
fi 

docker push ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/node3tierapp_web:latest
if [ $? -ne 0 ]; then 
   echo " ERROR pushing web image to aws "
   exit 1
fi 



num_instances=$( aws ec2 describe-instances | jq ".Reservations[].Instances[].InstanceId" | wc -l )


for i in {0..3}
do 
 aws ec2 describe-instances | jq ".Reservations[$i].Instances[$i].SecurityGoups[$i].Groupame , .Reservatins[$i].Instances[$i].InstanceId"  
done


exit 1 

num=$(($num_instances + 2 ))

while "$( aws autoscaling describe-auto-scaling-groups | jq ".AutoScalingGroups[]" )" -eq "null"


minsize=$( aws autoscaling describe-auto-scaling-groups | jq ".AutoScalingGroups[].MinSize" )
descap=$( aws autoscaling describe-auto-scaling-groups | jq ".AutoScalingGroups[].DesiredCapacity" )

aws autoscaling  update-auto-scaling-group --auto-scaling-group-name node3tierapp-${ENV}_asg --min-size $num_instances --max-size $num --desired-capacity $num

sleep 600

aws autoscaling  update-auto-scaling-group --auto-scaling-group-name node3tierapp-${ENV}_asg --min-size $minsize --max-size $num_instances --desired-capacity $descap


echo " Deploy done in the environment $env "



aws ec2 describe-instances  | jq ".Reservations[].Instances[].SecurityGoups[].Groupame , .Reservatins[].Instances[].InstanceId" | perl -p -e  's/\n/ /'
