#!/bin/bash

region=$1
id=$2
app_name=$3 
db_name=$4 
db_pass=$5 
env=$6


db_address=$( aws docdb describe-db-instances --region "$region" --db-instance-identifier "arn:aws:rds:$region:$id:db:$app_name" --query 'DBInstances[*].[DBInstanceIdentifier,Endpoint]'  | grep Address | awk '{ print $2 }' | tr -d '[",,]' )  


db_address_string="postgres://$4:$5@$db_address/$4"

echo -n  $db_address_string >/tmp/dbaddress$env



