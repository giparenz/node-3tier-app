#!/bin/bash

for i in {0..1}
do 
   aws autoscaling attach-load-balancer-target-groups --auto-scaling-group-name $1 --target-group-arns $( aws elbv2 describe-target-groups | jq ".TargetGroups[$i].TargetGroupArn" | tr -d '"')
done 

