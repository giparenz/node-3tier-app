#!/usr/bin/env bash
env=$1
alb_api=`aws elbv2 describe-load-balancers --names node3tierapp-${env}-api-lb | grep LoadBalancerArn | sed -e "s:.*loadbalancer/\(.*\)\",:\1:g" |tr -d '[:space:]'`
alb_web=`aws elbv2 describe-load-balancers --names node3tierapp-${env}-web-lb | grep LoadBalancerArn | sed -e "s:.*loadbalancer/\(.*\)\",:\1:g" |tr -d '[:space:]'`
targetgroup_api=`aws elbv2 describe-target-groups --names node3tierapp-${env}-tr-3000-gr |grep TargetGroupArn | sed -e "s:.*\:\(.*\)\",:\1:g" | tr -d '[:space:]'`
targetgroup_web=`aws elbv2 describe-target-groups --names node3tierapp-${env}-tr-80-gr |grep TargetGroupArn | sed -e "s:.*\:\(.*\)\",:\1:g" | tr -d '[:space:]'`

cat dashboards/dashboard.json | sed -e "s:APP_ALB_WEB:${alb_web}:g"  -e "s:APP_TARGET_WEB:${targetgroup_web}:g" -e "s:APP_ALB_API:${alb_api}:g"  -e "s:APP_TARGET_API:${targetgroup_api}:g" -e "s:ENV:${env}:g"


