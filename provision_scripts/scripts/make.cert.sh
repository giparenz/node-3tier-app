openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ../certs/privateKey.key -out ../certs/certificate.crt
openssl rsa -in ../certs/privateKey.key -check 
openssl x509 -in ../certs/certificate.crt -text -noout
openssl rsa -in ../certs/privateKey.key -text > ../certs/private.pem
openssl x509 -inform PEM -in ../certs/certificate.crt > ../certs/public.pem

