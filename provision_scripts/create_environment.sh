#!/usr/bin/env bash
echo -e "\n\n\n ------- Creating inital users and roles ----------\n\n\n"
ansible-playbook cicd_create.yml  -vvv 
if [ $? -ne 0 ]; then 
  echo "Creating inital users and roles >>>> FAILED " 
  exit 1 
fi 
echo "\n\n\n ------- Creating staging environment ----------\n\n\n"
ansible-playbook create_env.yml --extra-vars @env_config/staging.yml -vvv
if [ $? -ne 0 ]; then 
  echo "Creating staging environment >>>> FAILED "
  exit 1  
fi 
echo -e "\n\n\n ------- Creating staging dashboard web ----------\n\n\n"
./scripts/generate_dashboard.sh staging > dashboards/dashboard-staging.json
if [ $? -ne 0 ]; then 
  echo "Creating staging dashboard web >>>> FAILED " 
  exit 1 
fi 
echo -e "\n\n\n ------- putting dashboard on cloudwatch staging ----------\n\n\n"
aws cloudwatch put-dashboard --dashboard-name "node3tierapp-staging" --dashboard-body file://dashboards/dashboard-staging.json
if [ $? -ne 0 ]; then 
  echo "putting dashboard on cloudwatch staging >>>> FAILED " 
  exit 1 
fi 
echo -e "\n\n\n ------- Creating production environment ----------\n\n\n"
ansible-playbook create_env.yml --extra-vars @env_config/prod.yml
if [ $? -ne 0 ]; then 
  echo "Creating production environment >>>> FAILED " 
  exit 1 
fi 
echo -e "\n\n\n ------- Creating production dashboard ----------\n\n\n"
./scripts/generate_dashboard.sh prod > dashboards/dashboard-prod.json
if [ $? -ne 0 ]; then 
  echo "Creating production dashboard >>>> FAILED " 
  exit 1 
fi 
echo -e "\n\n\n ------- putting dashboard on cloudwatch prod ----------\n\n\n"
aws cloudwatch put-dashboard --dashboard-name "node3tierapp-prod" --dashboard-body file://dashboards/dashboard-prod.json
if [ $? -ne 0 ]; then 
  echo "putting dashboard on cloudwatch prod >>>> FAILED " 
  exit 1 
fi 
 
